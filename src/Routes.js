import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";

// Component
import News from "./pages/List";
import Login from "./pages/Login";

const Routes = () => {
  return (
    <Router>
      {/* Navigasi */}
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/news">News</Link>
            </li>
            <li>
              <Link to="/lain">Halaman lain</Link>
            </li>
          </ul>
        </nav>

        {/* Tempat mendaftarkan halaman */}
        <Switch>
          <Route path="/login">
            <Login />
          </Route>

          <Route path="/news">
            <News />
          </Route>

          <Route path="/lain">
            <HalamanLain />
          </Route>
        </Switch>

        <p>Bagian bawah yang selalu ada</p>
      </div>
    </Router>
  );
};

const HalamanLain = () => {
  return <h1>Ini Halaman lain</h1>;
};

export default Routes;
