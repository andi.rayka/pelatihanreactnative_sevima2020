import React, { useState, useEffect } from "react";
import styled from "styled-components";

// Function
import axios from "axios";
import moment from "moment";

axios.defaults.baseURL = "http://learn.hackatown.online/news-json.php";

// Bisa pakai props
const NewsItem = styled.div`
  background-color: ${({ bgColor }) => bgColor};
  padding: 15px;
  ${({ isLast }) => !isLast && "border-bottom: 1px solid #000;"}
`;

const NewsItemTitle = styled.p`
  color: #000;
  font-size: 16;
  margin: 0;
`;

const NewsItemDate = styled.p`
  color: #000;
  margin: 0;
`;

const List = () => {
  const [news, setNews] = useState([]);

  useEffect(() => {
    fetchNews();
  }, []);

  const fetchNews = async () => {
    try {
      const url = "news-json.php";

      const resp = await axios.get(url);

      setNews(resp.data);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <h1>Halaman list</h1>
      {news.map((item, key) => {
        return (
          <NewsItem bgColor="yellow" key={key} isLast={key === news.length - 1}>
            <NewsItemTitle>{item.title}</NewsItemTitle>
            <NewsItemDate>
              {moment(item.date).format("DD MMMM YYYY")}
            </NewsItemDate>
          </NewsItem>
        );
      })}
    </>
  );
};

export default List;
