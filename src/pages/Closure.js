import React from "react";

const Closure = () => {
  // Method dalam method
  const fungsi = () => {
    alert("ok");
  };

  // contoh currying
  const currying = x => {
    return y => {
      return z => {
        alert(x + y + z);
      };
    };
  };

  //   // cara panggil
  //   currying(10)(39)(49);

  //   // ----Sama dengan----

  //   // cara cepat
  //   const x_terisi = currying(10);
  //   // lebih singkat selanjutnya
  //   x_terisi(39, 40);

  return (
    <div>
      <button onClick={fungsi}>BENAR</button>
      <button onClick={() => alert("not ok")}>SALAH</button>
    </div>
  );
};

export default Closure;
