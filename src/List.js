import React, { useState } from "react";
import styled from "styled-components";

// Bisa pakai props
const NewsItem = styled.div`
  background-color: ${({ bgColor }) => bgColor};
  padding: 15px;
  ${({ isLast }) => !isLast && "border-bottom: 1px solid #999;"}
`;

const NewsItemTitle = styled.p`
  color: #000;
  font-size: 16;
  margin: 0;
`;

const NewsItemDate = styled.p`
  color: #000;
  margin: 0;
`;

const NewsContainer = styled.div`
  width: 1000px;
  padding: 15px;
  border: 1px solid #999;
  position: absolute;
  left: 25%;
  top: 10%;
  border-radius: 4px;
`;

const NewsList = styled.div`
  width: 480px;
  border: 1px solid #e3e3e3;
  float: left;
`;

const NewsList2 = styled.div`
  width: 500px;
  border: 1px solid #e3e3e3;
  float: right;
`;

const NewsSeacrhInput = styled.div`
  padding: 10px;
  background-color: #e3e3e3;
`;

const Input = styled.input`
  padding: 0.5em;
  background: #fff;
  border: 1px solid #999;
  border-radius: 3px;
  width: 440px;
  margin-bottom: 5px;
  margin-top: 5px;
`;

const ButtonAdd = styled.button`
  padding: 0.5em 2em;
  color: #fff;
  background: #2196f3;
  border: 1px solid #999;
  border-radius: 3px;
  margin-bottom: 10px;
  margin-top: 10px;
`;

const ButtonDiv = styled.div`
  text-align: center;
`;

const List = () => {
  const [news, setNews] = useState([
    {
      title: "judul 1",
      date: "12 Des 2019"
    },
    {
      title: "judul 2",
      date: "13 Des 2019"
    }
  ]);

  return (
    <NewsContainer>
      <div>
        <h2>To Do List</h2>
      </div>
      <NewsList>
        <NewsSeacrhInput>
          <Input type="text" placeholder="Search..." />
        </NewsSeacrhInput>

        <NewsSeacrhInput>
          <Input type="text" placeholder="Title" />
          <Input type="text" placeholder="Description" />
        </NewsSeacrhInput>
        <ButtonDiv>
          <ButtonAdd>Add</ButtonAdd>
        </ButtonDiv>
      </NewsList>

      <NewsList2>
        {news.map((item, key) => {
          return (
            <NewsItem bgColor="#e9e9e975" key={key} isLast={key === news.length - 1}>
              <NewsItemTitle>{item.title}</NewsItemTitle>
              <NewsItemDate>{item.date}</NewsItemDate>
            </NewsItem>
          );
        })}
      </NewsList2>
    </NewsContainer>

  )
};

export default List;
